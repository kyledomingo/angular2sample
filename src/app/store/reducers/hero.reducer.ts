import { HeroListActionTypes, HeroListActions } from '../actions/hero.actions';
import { Hero } from '../../models/hero';
import { isUndefined, isNull } from 'util';

export function reducer(state: Hero[], actions: HeroListActions) {

    switch (actions.type) {
        case HeroListActionTypes.ADD: {
            state.push(actions.payload);
            return state;
        }

        case HeroListActionTypes.REMOVE: {
            state.splice(state.indexOf(actions.payload), 1);
            return state;
        }

        default: {
            return state;
        }
    }
}
