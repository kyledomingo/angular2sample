import { PersonActionTypes, PersonListActions } from '../actions/person.actions';
import { Person } from '../models/person';
import { isUndefined, isNull } from 'util';

export function reducer(state: Person[], actions: PersonListActions) {

    switch (actions.type) {
        case PersonActionTypes.ADD: {
            state.push(actions.payload);
            return state;
        }

        case PersonActionTypes.REMOVE: {
            state.splice(state.indexOf(actions.payload), 1);
            return state;
        }

        default: {
            return state;
        }
    }
}
