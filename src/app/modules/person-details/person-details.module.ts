import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonDetailsComponent, PersonDetailDialog } from '../../components/person-details/person-details.component';
import { PersonListComponent } from '../../components/person-list/person-list.component';
import { ThemeModule } from '../../app.theme.module';

@NgModule({
  imports: [CommonModule, ThemeModule],
  exports: [PersonDetailsComponent],
  declarations: [PersonDetailsComponent, PersonDetailDialog],
  bootstrap: [PersonDetailsComponent],
  entryComponents: [PersonDetailDialog]
})
export class PersonDetailsModule {}
