import { Component, OnInit, Inject } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Hero } from '../../models/hero';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HeroService } from '../../services/hero.service';
import { isUndefined, isNull } from 'util';

@Component({
  selector: 'app-heroes',
  templateUrl: './layouts/heroes.component.html',
  styleUrls: ['./layouts/heroes.component.css'],
  providers: [HeroService]
})

export class HeroesComponent implements OnInit {

  heroes: Hero[];
  heroName: string;

  constructor(public dialog: MatDialog, private heroService: HeroService) {}

  ngOnInit() {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroes = [];
    this.heroService.getHeroes().subscribe(val => this.heroes = val);
  }

  removeHero(hero: Hero): void {
    this.heroService.removeHero(hero);
    this.getHeroes();
  }

  addHero(hero: Hero): void {
    this.heroService.addHero(hero);
    this.getHeroes();
  }

  showAddDialog(): void {
    const dialogRef = this.dialog.open(AddHeroDialogComponent, { data: { name: this.heroName } });
    dialogRef.afterClosed().subscribe(result => {

      if (isUndefined(result) || isNull(result)) {
        return;
      }

      const newHero: Hero = {
        name: result
      };

      this.addHero(newHero);
    });
  }
}

// Add dialog
@Component({
  selector: 'app-hero-dialog-add',
  templateUrl: './layouts/heroes.dialog.add.html'
})

export class AddHeroDialogComponent {
  constructor(public dialogRef: MatDialogRef<AddHeroDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
