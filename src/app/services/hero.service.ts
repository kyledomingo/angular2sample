import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as HeroActions from '../store/actions/hero.actions';

import { Hero } from '../models/hero';
// import { Action } from 'rxjs/scheduler/Action';

@Injectable()
export class HeroService {

  heroes$: Observable<Hero[]>;

  constructor(private store: Store<HeroActions.HeroListState>) {
    this.heroes$ = store.pipe(select('heroes'));
  }

  addHero(hero: Hero): void {
    this.store.dispatch(new HeroActions.AddHeroAction(hero));
  }

  getHeroes(): Observable<Hero[]> {
    return this.heroes$;
  }

  removeHero(hero: Hero): void {
    this.store.dispatch(new HeroActions.RemoveHeroAction(hero));
  }
}
