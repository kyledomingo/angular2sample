import { Action } from '@ngrx/store';
import { Person } from '../models/person';

export enum PersonActionTypes {
    INIT = 'Init',
    ADD = 'Add',
    REMOVE = 'Remove'
}

export class AddPersonAction implements Action {
    readonly type = PersonActionTypes.ADD;
    constructor(public payload: Person) {}
}

export class RemovePersonAction implements Action {
    readonly type = PersonActionTypes.REMOVE;
    constructor(public payload: Person) {}
}

export type PersonListActions = AddPersonAction | RemovePersonAction;

export interface PeopleState {
    people: Person[];
}
