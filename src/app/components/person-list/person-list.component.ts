import { Component, OnInit, Inject } from '@angular/core';
import { Store, select, ActionsSubject, Action } from '@ngrx/store';
import { Person } from '../../models/person';
import { PersonService } from '../../services/person.service';
import { PersonActionsSubject, AppActions } from '../../person-actions-subject';
import { PersonActionTypes } from '../../actions/person.actions';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css'],
  providers: [
    PersonService,
    AppActions,
    {provide: ActionsSubject, useClass: PersonActionsSubject}
  ]
})

export class PersonListComponent implements OnInit {

  people: Person[];

  constructor(private personService: PersonService, private actionListener: AppActions) {}

  ngOnInit() {
    this.list();
    
    let person: Person = new Person();
    console.log(person);

    this.actionListener.actionList().subscribe((action: Action) => {
      switch (action.type) {
        case PersonActionTypes.ADD:
        case PersonActionTypes.REMOVE:
          this.list();
          break;
      }
    });
  }

  list(): void {
    this.people = [];
    this.personService.list().subscribe(people => this.people = people);
  }

  remove(person: Person): void {
    this.personService.remove(person);
  }
}
