import { Action } from '@ngrx/store';
import { Hero } from '../../models/hero';

export enum HeroListActionTypes {
    INIT = '[HeroListAction] Init',
    ADD = '[HeroListAction] Add',
    REMOVE = '[HeroListAction] Remove'
}

export class AddHeroAction implements Action {
    readonly type = HeroListActionTypes.ADD;
    constructor(public payload: Hero) {}
}

export class RemoveHeroAction implements Action {
    readonly type = HeroListActionTypes.REMOVE;
    constructor(public payload: Hero) {}
}

export type HeroListActions = AddHeroAction | RemoveHeroAction;

export interface HeroListState {
    heroes: Hero[];
}
