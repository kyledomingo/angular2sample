import { Action } from '@ngrx/store';

export enum HeroListActions {
    APPEND = '[Hero] Append',
    REMOVE = '[Hero] Remove',
    RESET = '[Hero] Reset'
}

export class Append implements Action {
    readonly type = HeroListActions.APPEND;
}

export class Remove implements Action {
    readonly type = HeroListActions.REMOVE;
}

export type HeroListActionsTypes = Append | Remove;
