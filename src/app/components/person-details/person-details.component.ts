import { Component, OnInit, Inject} from '@angular/core';
import { PersonActionsSubject, AppActions } from '../../person-actions-subject';
import { PersonService } from '../../services/person.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { isUndefined, isNull } from 'util';
import { Person } from '../../models/person';

@Component({
  selector: 'app-person-details',
  templateUrl: './person-details.component.html',
  styleUrls: ['./person-details.component.css'],
  providers: [PersonService, AppActions, PersonActionsSubject]
})
export class PersonDetailsComponent implements OnInit {

  constructor(private service: PersonService, private listener: AppActions, private dialog: MatDialog) { }

  ngOnInit() {
  }

  showAddDialog() {
    console.log('Adding person details');
    const dialogRef = this.dialog.open(PersonDetailDialog, {data: new Person()});
    dialogRef.afterClosed().subscribe(result => {

      if (isUndefined(result) || isNull(result)) {
        return;
      }

      this.service.add(result);
    });
  }
}

@Component({
  selector: 'app-hero-dialog-add',
  templateUrl: './person.add.dialog.html'
})

export class PersonDetailDialog {
  constructor(public dialogRef: MatDialogRef<PersonDetailDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
