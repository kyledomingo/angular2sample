import { ActionsSubject, Action } from '@ngrx/store';
import { Subject } from 'rxjs/Subject';
import { Injectable } from '@angular/core';
import { Observer } from 'rxjs/Observer';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AppActions {
    
    private actions: Subject<Action> = new Subject<Action>();

    actionList() {
        return this.actions;
    }

    nextAction(action: Action) {
        this.actions.next(action);
    }
}

@Injectable()
export class PersonActionsSubject extends ActionsSubject {

    constructor(private actions: AppActions) {
        super();
    }
    
    next(action: Action) {
        super.next(action);
        this.actions.nextAction(action);
    }
}
