import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import * as PersonListActions from '../actions/person.actions';
import { Person } from '../models/person';

@Injectable()
export class PersonService {

  people$: Observable<Person[]>;

  constructor(private store: Store<PersonListActions.PeopleState>) {
    this.people$ = store.pipe(select('people'));
  }

  add(person: Person): void {
    this.store.dispatch(new PersonListActions.AddPersonAction(person));
  }

  remove(person: Person): void {
    this.store.dispatch(new PersonListActions.RemovePersonAction(person));
  }

  list(): Observable<Person[]> {
    return this.people$;
  }
}
