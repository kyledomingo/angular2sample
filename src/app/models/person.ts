import { v4 as uuid } from 'uuid';

export class Person {
    firstName: String;
    lastName: String;
    age: String;
    id: String;

    constructor() {
        let id = uuid();
        this.id = id.substring(0, id.indexOf('-'));
    }
}
