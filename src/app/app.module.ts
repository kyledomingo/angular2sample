import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { ThemeModule } from './app.theme.module';

import { Store, StoreModule, select } from '@ngrx/store';
import { reducer as PersonActionsReducer } from './reducers/person.reducer';

import { PersonListModule } from './modules/person-list/person-list.module';
import { PersonDetailsModule } from './modules/person-details/person-details.module';

import { AppComponent } from './app.component';
import { Person } from './models/person';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ThemeModule,
    PersonDetailsModule,
    PersonListModule,
    StoreModule.forRoot({people: PersonActionsReducer}, {
      initialState: {
        people: new Array<Person>()
      }
    })
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
