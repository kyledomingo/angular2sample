import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ThemeModule } from '../../app.theme.module';

import { HeroesComponent, AddHeroDialogComponent } from '../../components/heroes/heroes.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    ThemeModule
  ],
  declarations: [
    HeroesComponent,
    AddHeroDialogComponent
  ],
  entryComponents: [
    AddHeroDialogComponent
  ],
  bootstrap: [HeroesComponent]
})
export class HerolistModule { }
