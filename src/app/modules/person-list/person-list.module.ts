import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PersonDetailsModule } from '../person-details/person-details.module';
import { PersonListComponent } from '../../components/person-list/person-list.component';
import { PersonService } from '../../services/person.service';
import { ThemeModule } from '../../app.theme.module';

@NgModule({
  imports: [CommonModule, ThemeModule],
  exports: [PersonListComponent],
  declarations: [PersonListComponent],
  bootstrap: [PersonListComponent],
  providers: []
})

export class PersonListModule { }
